import math
import time
from numpy import dot
from numpy.linalg import norm
import numpy as np
from collections import deque
import scipy

def random_hv_py(num: int, dim=None, dtype=None, device=None):
    """
    Creates num random hypervectors of dim dimensions in the bipolar system.
    When dim is None, creates one hypervector of num dimensions.
    """
    if dim is None:
        hv = np.random.randint(0, 2, size=(num,))
    else:
        hv = np.random.randint(0, 2, size=(num, dim))
    hv[hv == 1] = -1
    hv[hv == 0] = 1
    return hv

def level_hv_py(num: int, dim: int, r=0.0, dtype=None, device=None):
    """
    Creates num random level correlated hypervectors of dim-dimensions in the bipolar system.
    Span denotes the number of approximate orthogonalities in the set (only 1 is an exact guarantee)
    """
    hv = np.zeros((num, dim), dtype=dtype)

    # convert from normilzed "randomness" variable r to number of orthogonal vectors sets "span"
    span = r * num + (1 - r) * 1
    # generate the set of orthogonal vectors within the level vector set
    span_hv = random_hv_py(int(math.ceil(span + 1)), dim, dtype=dtype, device=device)
    # for each span within the set create a treshold vector
    # the treshold vector is used to interpolate between the
    # two random vector bounds of each span.
    treshold_v = np.random.rand(int(math.ceil(span)), dim)

    levels_per_span: float = (num - 1) / span

    for i in range(num):
        span_idx = int(i // levels_per_span)

        # special case: if we are on a span border (e.g. on the first or last levels)
        # then set the orthogonal vector directly.
        # This also prevents an index out of bounds error for the last level
        # when treshold_v[span_idx], and span_hv[span_idx + 1] are not available.
        if abs(i % levels_per_span) < 1e-12:
            hv[i] = span_hv[span_idx]
        else:
            level_within_span = i % levels_per_span
            # the treshold value from the start hv's perspective
            t = 1 - (level_within_span / levels_per_span)

            span_start_hv = span_hv[span_idx]
            span_end_hv = span_hv[span_idx + 1]
            hv[i] = np.where(treshold_v[span_idx] < t, span_start_hv, span_end_hv)

    return hv


def circular_hv_py(num: int, dim: int, r=0.0, dtype=None, device=None):
    """
    Creates num random circular level correlated hypervectors
    of dim dimensions in the bipolar system.
    When dim is None, creates one hypervector of num dimensions.
    """
    hv = np.zeros((num, dim), dtype=dtype)

    # convert from normilzed "randomness" variable r to number of orthogonal vectors sets "span"
    span = r * num + (1 - r) * 1
    # generate the set of orthogonal vectors within the level vector set
    span_hv = random_hv_py(int(math.ceil(span + 1)), dim, dtype=dtype, device=device)
    # for each span within the set create a treshold vector
    # the treshold vector is used to interpolate between the
    # two random vector bounds of each span.
    treshold_v = np.random.rand(int(math.ceil(span)), dim)
    levels_per_span: float = num / span

    mutation_history = deque()

    hv[0] = span_hv[0]
    mutation_hv = span_hv[0]

    for i in range(1, num + 1):
        span_idx = int(i // levels_per_span)

        # special case: if we are on a span border (e.g. on the first or last levels)
        # then set the orthogonal vector directly.
        # This also prevents an index out of bounds error for the last level
        # when treshold_v[span_idx], and span_hv[span_idx + 1] are not available.
        if abs(i % levels_per_span) < 1e-12:
            temp_hv = span_hv[span_idx]

        else:
            level_within_span = i % levels_per_span
            # the treshold value from the start hv's perspective
            t = 1 - (level_within_span / levels_per_span)

            span_start_hv = span_hv[span_idx]
            span_end_hv = span_hv[span_idx + 1]
            temp_hv = np.where(treshold_v[span_idx] < t, span_start_hv, span_end_hv)

        mutation_history.append(temp_hv * mutation_hv)
        mutation_hv = temp_hv

        if i % 2 == 0:
            hv[i // 2] = mutation_hv

    for i in range(num + 1, num * 2 - 1):
        mut = mutation_history.popleft()
        mutation_hv *= mut

        if i % 2 == 0:
            hv[i // 2] = mutation_hv

    return hv


def bind_py(a: np.array, b: np.array, out=None):
    return np.multiply(a, b)


def similarity_py(a: np.array, b: np.array):
    return np.dot(a, b)/(norm(a)*norm(b))
