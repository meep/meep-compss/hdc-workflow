import os
import torch
import pandas as pd
from torch.utils.data import Dataset
import requests

DATA_URL = (
    "https://archive.ics.uci.edu/ml/machine-learning-databases/00194/sensor_readings_4.data"
)


class WallFollowing(Dataset):
    dir_name = "wallFollowing_simple"
    file = "wallFollowing_simple.csv"

    def __init__(
        self,
        root_dir,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        if os.path.isfile(os.path.join(dataset_dir, self.file)):
            return True

        return False

    def _load_data(self):
        path = os.path.join(self.root_dir, self.dir_name, self.file)

        data = pd.read_csv(
            path,sep=" ",
            names=["s0", "s1", "s2", "s3", "dir"],
            dtype={
                "s0": float, "s1": float, "s2": float, "s3": float, "dir": str,
            }).dropna()

        targets = data.filter(["dir"], axis=1)
        data = data.drop("dir", axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(DATA_URL, allow_redirects=True, stream=True)
        with open(os.path.join(dataset_dir, self.file), "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                f.write(chunk)

if __name__ == "__main__":
    d = WallFollowing('.', download=True)
    print(d.__getitem__(10))