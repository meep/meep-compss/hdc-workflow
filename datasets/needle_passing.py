import os
import torch
import pandas as pd
from torch.utils.data import Dataset
import requests
import numpy as np
import io
import zipfile

#TODO
# ARCHIVE_URL = "https://drive.google.com/uc?export=download&confirm=JzVx&id=1TgKIb8S8MQvbmmJyyUsgsasmJpq7vXzT"


class NeedlePassing(Dataset):
    dir_name = "needle-passing"
    doctors = ["B", "C", "D", "E", "F", "G", "H", "I"]
    num_trials = 5
    #got "ValueError: 10 is not in list", but 10 is not in the readme targets for needle passing (included 10 for now)
    num_classes = 9
    class_mapping = [1, 2, 3, 4, 5, 6, 8, 10, 11]

    def __init__(
        self,
        root_dir,
        doctors=None,
        trials=None,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.used_trials = trials if trials != None else list(range(self.num_trials))
        self.used_doctors = doctors if doctors != None else self.doctors

        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            raise NotImplemented("Download is not supported for this dataset")
            self.download()

        if not self._check_exists():
            raise FileNotFoundError(
                f"Folder in {root_dir} named {self.dir_name} was not found"
            )

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        sample = self.data[idx]
        targets = self.targets[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            targets = self.target_transform(targets)

        return sample, targets

    def get_data_filename(self, doctor, trial_idx):
        return os.path.join(
            self.root_dir,
            self.dir_name,
            "kinematics",
            "AllGestures",
            f"Needle_Passing_{doctor}00{trial_idx + 1}.txt",
        )

    def get_target_filename(self, doctor, trial_idx):
        return os.path.join(
            self.root_dir,
            self.dir_name,
            "transcriptions",
            f"Needle_Passing_{doctor}00{trial_idx + 1}.txt",
        )

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        return True

    def _load_data(self):
        all_data_dfs = []

        for trail_idx in self.used_trials:
            for doctor in self.used_doctors:

                target_file = self.get_target_filename(doctor, trail_idx)
                if not os.path.isfile(target_file):
                    continue

                target_df = pd.read_csv(
                    target_file,
                    delim_whitespace=True,
                    names=["start", "end", "class"],
                    index_col=False,
                )

                data_file = self.get_data_filename(doctor, trail_idx)
                if not os.path.isfile(data_file):
                    continue

                data_df = pd.read_csv(
                    data_file,
                    delim_whitespace=True,
                    dtype=float,
                    header=None,
                    index_col=False,
                )

                targets = np.full(data_df.shape[0], np.NaN)

                for _, row in target_df.iterrows():
                    start_bound = row["start"]
                    end_bound = row["end"]

                    global_target = int(row["class"][1:])
                    target = self.class_mapping.index(global_target)
                    targets[start_bound:end_bound] = target

                data_df["target"] = targets
                all_data_dfs.append(data_df)

        all_data_df = pd.concat(all_data_dfs, ignore_index=True)
        # drop all the frames that have no target
        all_data_df = all_data_df.dropna()

        targets = all_data_df.filter(["target"], axis=1)
        targets = torch.tensor(targets.values, dtype=torch.int64)
        targets = targets.squeeze(1)

        data = all_data_df.drop(["target"], axis=1)
        data = torch.tensor(data.values, dtype=torch.float32)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)
