import os
import io
# import torch
import pandas as pd
import numpy as np
# from torch.utils import data
# from torch.utils.data import Dataset
import zipfile
import requests
from datetime import datetime

ARCHIVE_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/00501/PRSA2017_Data_20130301-20170228.zip"


class Beijing():
    dir_name = "beijing"
    file = os.path.join(
        "PRSA_Data_20130301-20170228", "PRSA_Data_Aotizhongxin_20130301-20170228.csv"
    )

    def __init__(
        self,
        root_dir,
        target_label="TEMP",
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.target_label = target_label
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        #if torch.is_tensor(idx):
        #    idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    @property
    def num_classes(self):
        return self.targets.max().item() - self.targets.min().item() + 1

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        if os.path.isfile(os.path.join(dataset_dir, self.file)):
            return True

        return False

    def _datetime(self, year, month, day, hour):
        date_str = f"{day}/{month}/{year} {hour}"
        return pd.to_datetime(date_str, format="%d/%m/%Y %H")

    def _load_data(self):
        path = os.path.join(self.root_dir, self.dir_name, self.file)

        data = pd.read_csv(
            path,
            dtype={
                "PM2.5": float,
                "PM10": float,
                "SO2": float,
                "NO2": float,
                "CO": float,
                "O3": float,
                "TEMP": float,
                "PRES": float,
                "DEWP": float,
                "RAIN": float,
                "WSPM": float,
            },
            parse_dates={"date": ["year", "month", "day", "hour"]},
            date_parser=self._datetime,
        )

        data = data.dropna()

        targets = data.filter([self.target_label], axis=1)
        data = data.drop(["No", "station", self.target_label], axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)

