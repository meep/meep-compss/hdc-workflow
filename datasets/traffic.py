import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils import data
from torch.utils.data import Dataset
import zipfile
import requests
from datetime import datetime

ARCHIVE_URL = "http://archive.ics.uci.edu/ml/machine-learning-databases/00483/Behavior%20of%20the%20urban%20traffic%20of%20the%20city%20of%20Sao%20Paulo%20in%20Brazil.zip"


class Traffic(Dataset):
    dir_name = "traffic"
    file = os.path.join(
        "Behavior of the urban traffic of the city of Sao Paulo in Brazil",
        "Behavior of the urban traffic of the city of Sao Paulo in Brazil.csv",
    )

    def __init__(
        self,
        root_dir,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    @property
    def num_classes(self):
        return self.targets.max().item() - self.targets.min().item() + 1

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        if os.path.isfile(os.path.join(dataset_dir, self.file)):
            return True

        return False

    def _datetime(self, index, hour_idx):
        dates = []
        for i, h in zip(index.tolist(), hour_idx.tolist()):
            day = int((i // 27) + 14)
            hour = int(((h - 1) // 2) + 7)
            minutes = int((((h - 1) / 2) % 1) * 60)
            date_str = f"{day}/12/2009 {hour}:{minutes}"
            dates.append(pd.to_datetime(date_str, format="%d/%m/%Y %H:%M"))

        return dates

    def _load_data(self):
        path = os.path.join(self.root_dir, self.dir_name, self.file)

        data = pd.read_csv(
            path,
            sep=";",
            decimal=",",
            dtype={
                "Hour (Coded)": int,
                "Immobilized bus": int,
                "Broken Truck": int,
                "Vehicle excess": int,
                "Accident victim": int,
                "Running over": int,
                "Fire vehicles": int,
                "Occurrence involving freight": int,
                "Incident involving dangerous freight": int,
                "Lack of electricity": int,
                "Fire": int,
                "Point of flooding": int,
                "Manifestations": int,
                "Defect in the network of trolleybuses": int,
                "Tree on the road": int,
                "Semaphore off": int,
                "Intermittent Semaphore": int,
                "Slowness in traffic (%)": float,
            },
        )
        data["date"] = self._datetime(data.index, data["Hour (Coded)"])

        targets = data.filter(["Slowness in traffic (%)"], axis=1)
        data = data.drop(["Hour (Coded)", "Slowness in traffic (%)"], axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)
