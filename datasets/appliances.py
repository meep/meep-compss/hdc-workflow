import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import zipfile
import requests

ARCHIVE_URL = "http://archive.ics.uci.edu/ml/machine-learning-databases/00374/energydata_complete.csv"


class Appliances(Dataset):
    dir_name = "appliances"
    file = "energydata_complete.csv"

    def __init__(
        self,
        root_dir,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        file_exists = os.path.isfile(os.path.join(dataset_dir, self.file))
        if file_exists:
            return True

        return False

    def _load_data(self):
        file = self.file

        path = os.path.join(self.root_dir, self.dir_name, file)

        data = pd.read_csv(
            path,
            dtype={
                "Appliances": int,
                "lights": int,
                "T1": float,
                "RH_1": float,
                "T2": float,
                "RH_2": float,
                "T3": float,
                "RH_3": float,
                "T4": float,
                "RH_4": float,
                "T5": float,
                "RH_5": float,
                "T6": float,
                "RH_6": float,
                "T7": float,
                "RH_7": float,
                "T8": float,
                "RH_8": float,
                "T9": float,
                "RH_9": float,
                "T_out": float,
                "Press_mm_hg": float,
                "RH_out": float,
                "Windspeed": float,
                "Visibility": float,
                "Tdewpoint": float,
                "rv1": float,
                "rv2": float,
                "data": str,
            },
            parse_dates=["date"],
        )

        targets = data.filter(["Appliances"], axis=1)
        data = data.drop("Appliances", axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        filename = os.path.join(self.root_dir, self.dir_name, self.file)
        with open(filename, "wb") as f:
            f.write(r.content)
