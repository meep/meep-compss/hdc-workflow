import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils import data
from torch.utils.data import Dataset
import zipfile
import requests
from datetime import datetime

ARCHIVE_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/00247/data_akbilgic.xlsx"


class Stock(Dataset):
    dir_name = "stock"
    file = "data_akbilgic.xlsx"


    def __init__(
        self,
        root_dir,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target


    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        if os.path.isfile(os.path.join(dataset_dir, self.file)):
            return True

        return False

    def _load_data(self):
        path = os.path.join(self.root_dir, self.dir_name, self.file)

        data = pd.read_excel(
            path,
            header=1,
            parse_dates=["date"],
        )

        targets = data.filter(["ISE"], axis=1)
        data = data.drop(["ISE"], axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        filename = os.path.join(self.root_dir, self.dir_name, self.file)
        with open(filename, "wb") as f:
            f.write(r.content)