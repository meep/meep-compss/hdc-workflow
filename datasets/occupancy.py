import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import zipfile
import requests

ARCHIVE_URL = (
    "https://archive.ics.uci.edu/ml/machine-learning-databases/00357/occupancy_data.zip"
)


class Occupancy(Dataset):
    dir_name = "occupancy"
    train_file = "datatraining.txt"
    val_file = "datatest.txt"
    test_file = "datatest2.txt"

    def __init__(
        self,
        root_dir,
        partition="train",
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.partition = partition
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    @property
    def num_classes(self):
        return self.targets.max().item() - self.targets.min().item() + 1

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        file1_exists = os.path.isfile(os.path.join(dataset_dir, self.train_file))
        file2_exists = os.path.isfile(os.path.join(dataset_dir, self.val_file))
        file3_exists = os.path.isfile(os.path.join(dataset_dir, self.test_file))
        if file1_exists and file2_exists and file3_exists:
            return True

        return False

    def _load_data(self):
        if self.partition == "train":
            file = self.train_file
        elif self.partition == "val":
            file = self.val_file
        elif self.partition == "test":
            file = self.test_file

        path = os.path.join(self.root_dir, self.dir_name, file)

        data = pd.read_csv(
            path,
            dtype={
                "Occupancy": int,
                "HumidityRatio": float,
                "CO2": float,
                "Light": float,
                "Humidity": float,
                "Temperature": float,
                "data": str,
            },
            parse_dates=["date"],
        )

        targets = data.filter(["Occupancy"], axis=1)
        data = data.drop("Occupancy", axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)
