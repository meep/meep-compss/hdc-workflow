import os
import torch
import numpy as np
from torch.utils.data import Dataset
import scipy.io
import threading
import requests
""

def download(link, filelocation):
    r = requests.get(link, stream=True)
    with open(filelocation, "wb") as f:
        for chunk in r.iter_content(1024):
            if chunk:
                f.write(chunk)


def async_download(link, filelocation):
    thread = threading.Thread(target=download, args=(link, filelocation))
    thread.start()
    return thread


def get_dataset_url(subject, session):
    return f"http://bnci-horizon-2020.eu/database/data-sets/013-2015/Subject0{subject}_s{session}.mat"


def get_filename(subject, session):
    return f"Subject0{subject}_s{session}.mat"


class EEG(Dataset):
    dir_name = "eeg"
    wrong_types = {6, 9}  # cursor moves away from the box
    correct_types = {5, 10}  # cursor moves towards the box

    def __init__(
        self,
        root_dir,
        subject=1,
        partition="train",
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.subject = subject
        self.partition = partition
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data[idx]
        target = self.targets[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    @property
    def num_classes(self):
        return self.targets.max().item() - self.targets.min().item() + 1

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        return True

    def _load_data(self):
        session = 1 if self.partition == "train" else 2
        filename = get_filename(self.subject, session)
        path = os.path.join(self.root_dir, self.dir_name, filename)

        run = scipy.io.loadmat(path)["run"]

        data, targets = [], []
        for block in range(10):
            header_data = run[0][block]["header"]

            # Create a list of node labels indicating the location of the electrode
            raw_labels = header_data[0][0][0]["Label"][0]
            # Transform the numpy array into a list of strings
            self.labels = np.concatenate(raw_labels.reshape(-1)).tolist()
            # Drop the last label "Status"
            self.labels = self.labels[:-1]

            eeg_data = run[0][block]["eeg"][0][0]
            eeg_data = eeg_data - eeg_data.mean(axis=0, keepdims=True)

            events = header_data[0][0][0]["EVENT"][0][0]
            pos = events["POS"][0].reshape(-1).tolist()
            typ = events["TYP"][0].reshape(-1).tolist()

            for i, (position, event_type) in enumerate(zip(pos, typ)):
                if event_type in self.wrong_types or event_type in self.correct_types:
                    start = position
                    end = pos[i + 1] if i != (len(pos) - 1) else len(eeg_data)
                    # Cap at 320 samples which is ~600ms of samples at 512 Hz
                    # And is also divisible by 8
                    end = min(start + 320, end)
                    # Take the slice of eeg data that corresponds to this event
                    data.append(eeg_data[start:end].T)

                    y = 0 if event_type in self.correct_types else 1
                    targets.append(y)
    
        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        threads = []
        # There are 6 subjects with 2 sessions each
        for subject in range(1, 6 + 1):
            for session in range(1, 2 + 1):
                path = os.path.join(dataset_dir, get_filename(subject, session))
                thread = async_download(get_dataset_url(subject, session), path)
                threads.append(thread)

        for thread in threads:
            thread.join()
