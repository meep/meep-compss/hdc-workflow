import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import zipfile
import requests
from datetime import datetime

ARCHIVE_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/00235/household_power_consumption.zip"


class Consumption(Dataset):
    dir_name = "consumption"
    file = "household_power_consumption.txt"

    def __init__(
        self,
        root_dir,
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        file_exists = os.path.isfile(os.path.join(dataset_dir, self.file))
        if file_exists:
            return True

        return False

    def _dateparse(self, d, t):
        dt = d + " " + t
        return datetime.strptime(dt, "%d/%m/%Y %H:%M:%S")

    def _load_data(self):
        file = self.file

        path = os.path.join(self.root_dir, self.dir_name, file)

        data = pd.read_csv(
            path,
            sep=";",
            dtype={
                "Global_active_power": str,
                "Global_reactive_power": str,
                "Voltage": str,
                "Global_intensity": str,
                "Sub_metering_1": str,
                "Sub_metering_2": str,
                "Sub_metering_3": str,
                "data": str,
            },
            parse_dates={"datetime": ["Date", "Time"]},
            date_parser=self._dateparse,
        )

        targets = data.filter(["Global_active_power"], axis=1)
        data = data.drop("Global_active_power", axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)


if __name__ == "__main__":
    d = Consumption(".", download=True)
    print(d.__getitem__(10))
