import os
import io
import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
import zipfile
import requests
from datetime import datetime

ARCHIVE_URL = (
    "http://archive.ics.uci.edu/ml/machine-learning-databases/00360/AirQualityUCI.zip"
)


class AirQuality(Dataset):
    dir_name = "air-quality"
    file = "AirQualityUCI.csv"

    def __init__(
        self,
        root_dir,
        target_label="CO(GT)",
        transform=None,
        target_transform=None,
        download=False,
    ):
        self.root_dir = root_dir
        self.target_label = target_label
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        self.data, self.targets = self._load_data()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = self.data.iloc[idx]
        target = self.targets.iloc[idx]

        if self.transform:
            sample = self.transform(sample)

        if self.target_transform:
            target = self.target_transform(target)

        return sample, target

    @property
    def num_classes(self):
        return self.targets.max().item() - self.targets.min().item() + 1

    def _check_exists(self):
        if not os.path.isdir(self.root_dir):
            return False

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        if not os.path.isdir(dataset_dir):
            return False

        if os.path.isfile(os.path.join(dataset_dir, self.file)):
            return True

        return False

    def _dateparse(self, d, t):
        if pd.isna(d) or pd.isna(t):
            return float("nan")

        dt = d + " " + t
        return datetime.strptime(dt, "%d/%m/%Y %H.%M.%S")

    def _load_data(self):
        path = os.path.join(self.root_dir, self.dir_name, self.file)

        data = pd.read_csv(
            path,
            sep=";",
            decimal=",",
            dtype={
                "CO(GT)": float,
                "PT08.S1(CO)": float,
                "NMHC(GT)": float,
                "C6H6(GT)": float,
                "PT08.S2(NMHC)": float,
                "NOx(GT)": float,
                "PT08.S3(NOx)": float,
                "NO2(GT)": float,
                "PT08.S4(NO2)": float,
                "PT08.S5(O3)": float,
                "T": float,
                "RH": float,
                "AH": float,
                "Date": str,
                "Time": str,
            },
            parse_dates={"datetime": ["Date", "Time"]},
            date_parser=self._dateparse,
        )
        data = data.loc[:, ~data.columns.str.contains("^Unnamed")]
        data = data.dropna(axis=0, how="all")

        targets = data.filter([self.target_label], axis=1)
        data = data.drop(self.target_label, axis=1)

        return data, targets

    def download(self):
        """Download the data if it doesn't exist already."""

        if self._check_exists():
            return

        dataset_dir = os.path.join(self.root_dir, self.dir_name)
        os.makedirs(dataset_dir, exist_ok=True)

        r = requests.get(ARCHIVE_URL, allow_redirects=True)
        zip_bytes = io.BytesIO(r.content)
        with zipfile.ZipFile(zip_bytes, "r") as zip_ref:
            zip_ref.extractall(dataset_dir)
