import time
import hd.functional as HDF
import sklearn.metrics
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
from datasets.beijing import Beijing


D = 10000  # number of hypervector dimensions
TRAIN_SPLIT = 0.7  # 70% of the data is used for training 30% for testing
NUM_TARGET_LEVELS = 100

def transform_py(x):
    day = min(x.date.dayofyear - 1, 364)
    year = x.date.year - 2013
    hour = x.date.hour
    return np.array([year, day, hour], dtype=np.long)


def target_transform_py(y):
    return np.array(y.values, dtype=np.float)


def prepareData():
    ds_py = Beijing("data", target_label="TEMP")

    ds_py.transform = transform_py
    ds_py.target_transform = target_transform_py

    split_py = int((len(ds_py) * TRAIN_SPLIT) // 1)
    train_data_py = []
    test_data_py = []
    for i in range(len(ds_py)):
        if i < split_py:
            train_data_py.append(ds_py[i])
        else:
            test_data_py.append(ds_py[i])
    return ds_py, train_data_py, test_data_py


def encode_py(x, y, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py, target_max_py):
    year, day, hour = x
    date_vec_py = HD_day_py[day] * HD_year_py[year] * HD_hour_py[hour]

    temp_py = y
    temp_py = np.maximum(temp_py, target_min_py)
    temp_py = np.minimum(temp_py, target_max_py)

    temp_fraq_py = (temp_py - target_min_py) / (target_max_py - target_min_py)
    temp_index_py = int(np.round(temp_fraq_py * (NUM_TARGET_LEVELS - 1)))
    temp_vec_py = HD_target_py[temp_index_py]
    return (date_vec_py, temp_vec_py)


def train(train_data_py, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py,
                                           target_max_py, HD_mem_py):
    for x, y in train_data_py:
        (date_vec_py, temp_vec_py) = encode_py(x, y, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py,
                                               target_max_py)
        HD_mem_py += HDF.bind_py(date_vec_py, temp_vec_py)
    return HD_mem_py


def fit(test_data_py, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py, target_max_py, HD_mem_py, y_pred_py, y_true_py):
    for i, (x, y) in enumerate(test_data_py):
        date_vec_py, _ = encode_py(x, y, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py,
                                           target_max_py)
        temp_vec_py = HDF.bind_py(HD_mem_py[0], date_vec_py, [])
        dist = HDF.similarity_py(HD_target_py, temp_vec_py)
        pred = dist.argmax()
        pred_temp = (target_max_py - target_min_py) / float(
            NUM_TARGET_LEVELS
        ) * pred + target_min_py

        y_pred_py[i] = y
        y_true_py[i] = pred_temp
    return sklearn.metrics.mean_squared_error(y_true_py, y_pred_py)


def preprocess(hv_type, r):
    if hv_type == "Random":
        exp_hv_gen_py = HDF.random_hv_py
    elif hv_type == "Level":
        exp_hv_gen_py = HDF.level_hv_py
    elif hv_type == "Circular":
        def exp_hv_gen_py(num, dim, **kwargs):
            return HDF.circular_hv_py(num, dim, r, **kwargs)

    HD_hour_py = exp_hv_gen_py(24, D, dtype=np.float)
    HD_day_py = exp_hv_gen_py(365, D, dtype=np.float)
    # Since we do not conciders years to be within a circle, always use level
    HD_year_py = HDF.level_hv_py(5, D, dtype=np.float)
    # Since we do not conciders the temperature to be within a circle, always use level
    HD_target_py = HDF.level_hv_py(NUM_TARGET_LEVELS, D, dtype=np.float)

    HD_mem_py = np.zeros((1, D), dtype=np.float)
    return HD_day_py, HD_year_py, HD_hour_py, HD_target_py, HD_mem_py


if __name__ == "__main__":
    repeats = 1
    inittime = time.time()
    results = []

    ds_py, train_data_py, test_data_py = prepareData()

    target_min_py = np.array(ds_py.targets.min().item(), dtype=np.float)
    target_max_py = np.array(ds_py.targets.max().item(), dtype=np.float)

    predictions = []
    count = 0

    for _ in range(repeats):
        for hv_type in ["Random","Circular","Level"]:
            for r in (
                [
                    0,
                    0.0025,
                    0.005,
                    0.0075,
                    0.01,
                    0.015,
                    0.02,
                    0.025,
                    0.03,
                    0.04,
                    0.05,
                    0.1,
                    0.2,
                    0.3,
                    0.5,
                    0.75,
                    1,
                ]
                if hv_type == "Circular"
                else [0]
            ):
                HD_day_py, HD_year_py, HD_hour_py, HD_target_py, HD_mem_py = preprocess(hv_type, r)
                y_pred_py = np.zeros((len(test_data_py),), dtype=np.float)
                y_true_py = np.zeros((len(test_data_py),), dtype=np.float)
                HD_MEM = train(train_data_py, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py, target_max_py, HD_mem_py)

                mse = fit(test_data_py, HD_day_py, HD_year_py, HD_hour_py, HD_target_py, target_min_py, target_max_py,
                          HD_MEM, y_pred_py, y_true_py)
                print("Res " + str(hv_type) + " " + str(r) + " " + str(mse))
    print("Total time: " + str(time.time()-inittime))




